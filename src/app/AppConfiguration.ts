import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Injectable()
export class AppConfiguration {
  title: string;
  fdpurl: string;
  fdpemail: string;
  fdppassword: string;
  elasticurl: string;
  smartapiurl: string;
  
  constructor(private httpClient: HttpClient){};

public ensureInit(): Promise<any> {
    return new Promise((r, e) => {  
        this.httpClient.get("http://"+environment.apiurl+"/setting/load").subscribe((content: AppConfiguration) => {Object.assign(this, content);r(this);}, reason => e(reason));
    });
  };

}

