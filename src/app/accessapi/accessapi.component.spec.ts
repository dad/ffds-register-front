import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessapiComponent } from './accessapi.component';

describe('AccessapiComponent', () => {
  let component: AccessapiComponent;
  let fixture: ComponentFixture<AccessapiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessapiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessapiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
