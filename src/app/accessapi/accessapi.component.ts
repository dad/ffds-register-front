import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { FileSaverService } from 'ngx-filesaver';
import { FormControl} from '@angular/forms';
import { FormGroup} from '@angular/forms';


@Component({
  selector: 'app-accessapi',
  templateUrl: './accessapi.component.html',
  styleUrls: ['./accessapi.component.scss']
})
export class AccessapiComponent implements OnInit {


 public fileName: string;
  public importFile: File;

 Form = new FormGroup({
           title: new FormControl('', Validators.required),
           description: new FormControl(''),
           version: new FormControl(''),
           url: new FormControl(''),
           urldescription: new FormControl(''),
           paths: new FormControl(''),
           verb: new FormControl(''),
           summary: new FormControl(''),
           verbdescription: new FormControl(''),
           responses: new FormControl(''),
           responsedescription: new FormControl(''),
           content: new FormControl(''),
           schema: new FormControl(''),
           items: new FormControl(''),
           filetofill: new FormControl(),
  });


  constructor(
  private _httpClient: HttpClient,
  private _FileSaverService: FileSaverService,
  ) { }

  ngOnInit() {
  }

  swaggertoyaml(buttonType) {
if (buttonType == 'submit') {
let data: string;

data ='openapi: 3.0.0 \n\
info:  \n\
  title: '+ this.Form.value.title +' \n\
  description: '+ this.Form.value.description +' \n\
  version: '+ this.Form.value.version +' \n\
server:  \n\
  - url: '+ this.Form.value.url +' \n\
    description: '+ this.Form.value.urldescription +' \n\
paths:  \n\
      '+this.Form.value.paths+': \n\
        '+ this.Form.value.verb +': \n\
          summary: '+ this.Form.value.summary +' \n\
          description: '+ this.Form.value.verbdescription +' \n\
          responses: \n\
            '+ this.Form.value.responses +': \n\
              description: '+ this.Form.value.responsedescription +' \n\
              content: \n\
               '+ this.Form.value.content +': \n\
                schema: \n\
                  type: '+ this.Form.value.schema +' \n\
                  items: \n\
                  type: '+ this.Form.value.items +' \n\
'

    const fileName = `swagger.yaml`;
    const fileType = this._FileSaverService.genType(fileName);
    const txtBlob = new Blob([data], { type: fileType });

    this._FileSaverService.save(txtBlob, fileName);
  }

  if (buttonType == 'fill') {
     const files = (event.target as HTMLInputElement).files;
     this.importFile = files[0];

    let text = "";
    let lines = [];
    let line = "";
    let map = new Map();
    let fileReader = new FileReader();
    fileReader.onload = (event) => {
    text = fileReader.result as string;

      lines = text.split("\n");
      let j = 1;
      for ( let i = 0; i < lines.length; i++) {
           line = lines[i].split(": ");
           if (line[0].trim() == "description") {
           map.set("description"+j,line[1]);
           j++;
           } else {
           map.set(line[0].trim(),line[1]);
           }
           console.log(line);
      }

console.log(map);

      this.Form.setValue({
        title:  map.get('title'),
        description:  map.get('description1'),
        version:  map.get('version'),
        url:  map.get('- url'),
        urldescription:  map.get('description2'),
        paths:  '',
        verb:  '',
        summary:  map.get('summary'),
        verbdescription:  map.get('description3'),
        responses:  '',
        responsedescription:  map.get('description4'),
        content:  '',
        schema:  map.get('schema'),
        items:  map.get('items'),
        filetofill: ''
      });

    }
    fileReader.readAsText(this.importFile);


   }

  }

}
