import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccountLoginComponent } from './login/account.login.component';
import { NbAlertModule,NbCardModule, NbCheckboxModule,NbButtonModule, NbInputModule } from '@nebular/theme';
import { AccountAdminComponent } from './admin/account.admin.component';

  

@NgModule({
  declarations: [AccountLoginComponent, AccountAdminComponent],
  imports: [
    CommonModule,
    NbAlertModule,
    NbCheckboxModule,
    FormsModule,
    NbButtonModule,
    NbInputModule,
    ReactiveFormsModule,
    NbCardModule
      ],
})
export class AccountModule {
  
 }
