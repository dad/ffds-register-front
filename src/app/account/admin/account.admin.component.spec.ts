import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Account.AdminComponent } from './account.admin.component';

describe('Account.AdminComponent', () => {
  let component: Account.AdminComponent;
  let fixture: ComponentFixture<Account.AdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Account.AdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Account.AdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
