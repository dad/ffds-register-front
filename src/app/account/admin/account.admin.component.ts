import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-account.admin',
  templateUrl: './account.admin.component.html',
  styleUrls: ['./account.admin.component.scss']
})
export class AccountAdminComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }
  
  logout(){
    this.authService.logout();
    this.router.navigateByUrl('/auth/login');
  }

}
