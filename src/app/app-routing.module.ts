import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { RepositoryComponent } from './repository/repository.component';
import { AccessapiComponent } from './accessapi/accessapi.component';
import { DatasetsComponent } from './datasets/datasets.component';
import { OntologieComponent } from './ontologie/ontologie.component';
import { PublishComponent } from './publish/publish.component';
import { SettingfdpComponent } from './settingfdp/settingfdp.component';
import { SettingsmartapiComponent } from './settingsmartapi/settingsmartapi.component';
import { RepositoryinfoComponent } from './repositoryinfo/repositoryinfo.component';
import { SwaggereditorComponent } from './swaggereditor/swaggereditor.component';
import { ElasticsearchComponent } from './elasticsearch/elasticsearch.component'
import { PublishApiComponent } from './publishapi/publishapi.component'
import { SearchComponent } from './search/search.component';
import { AccountLoginComponent } from './account/login/account.login.component';
import { AppComponent } from './app.component';
import { StatsComponent } from './stats/stats.component';


const routes: Routes = [
                        {path: '', component : StatsComponent},
                        {path: 'login', component : LoginComponent},
                        {path: 'user', component : UserComponent},
                        {path: 'repository', component : RepositoryComponent},
                        {path: 'repositoryinfo', component : RepositoryinfoComponent},
                        {path: 'accessapi', component : AccessapiComponent},
                        {path: 'datasets', component : DatasetsComponent},
                        {path: 'ontologie', component : OntologieComponent},
                        {path: 'publish', component : PublishComponent},
                        {path: 'settingfdp', component : SettingfdpComponent},
                        {path: 'settingsmartharvester', component : SettingsmartapiComponent},
                        {path: 'swaggerapi', component : SwaggereditorComponent},
                        {path: 'elasticsearch', component : ElasticsearchComponent},
                        {path: 'publishapi', component : PublishApiComponent},
                        {path: 'simplesearch', component : SearchComponent},
                        {path: 'advancedsearch', component : SearchComponent},
                        {path: 'auth/login',component: AccountLoginComponent},
                       // {path: 'auth/admin', component: AdminComponent, canActivate: [AuthGuard] }
                        ]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
