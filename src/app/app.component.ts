import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbMenuItem, NbSidebarService } from '@nebular/theme';
import { AppConfiguration } from './AppConfiguration';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  title = 'register-front';

  settingtitle =   this.appConfig.title ;

  items: NbMenuItem[] = [
    {
      title: 'Home',
      icon: 'home-outline',
      link: '/',
      pathMatch:'full'
    },
    {
      title: 'Integrate your repository',
      icon: 'cube-outline',

      children: [
        {
          title: 'SDT information',
          link: '/repositoryinfo',
          pathMatch:'full',
          icon: 'flash-outline',
        },
        {
          title: 'Technical information',
          link: '/repository',
          pathMatch:'full',
          icon: 'text-outline',
        },
        {
          title: 'Describe access to datasets',
          icon: 'clipboard-outline',
          link: '/publishapi',
          pathMatch:'full'
        }
      ],
    },
    {
      title: 'Search',
      icon: 'search-outline',
      link: '/simplesearch',
      pathMatch:'full'
    },
    {
      title: 'Settings',
      icon: 'options-2-outline',
      children: [
        {
          title: 'FFDS settings',
          link: '/settingfdp',
          pathMatch:'full'
        },
        {
          title: 'SmartHarvester settings',
          link: '/settingsmartharvester',
          pathMatch:'full'
        }
      ],
    },
  ];

  constructor(
    private readonly sidebarService: NbSidebarService,
    private appConfig: AppConfiguration
    ) {}

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true);
    return false;
  }


}



