import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { RepositoryComponent } from './repository/repository.component';
import { AccessapiComponent } from './accessapi/accessapi.component';
import { DatasetsComponent } from './datasets/datasets.component';
import { OntologieComponent } from './ontologie/ontologie.component';
import { PublishComponent } from './publish/publish.component';
import { SettingfdpComponent } from './settingfdp/settingfdp.component';
import { SettingsmartapiComponent } from './settingsmartapi/settingsmartapi.component';
import { UserComponent } from './user/user.component';

import { ReactiveFormsModule } from '@angular/forms';
import {FormsModule} from '@angular/forms';

import { RepositoryinfoComponent } from './repositoryinfo/repositoryinfo.component';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FileSaverModule } from 'ngx-filesaver';
import { SwaggereditorComponent } from './swaggereditor/swaggereditor.component';
import { AppConfiguration } from './AppConfiguration';
import { ElasticsearchComponent } from './elasticsearch/elasticsearch.component';
import { PublishApiComponent } from './publishapi/publishapi.component';
import { SearchComponent } from './search/search.component'
import { ParseXmlService } from './services/parse-xml.service';
import { NbMenuModule, 
  NbThemeModule,NbStepperModule,
  NbCardModule, NbSidebarModule, NbLayoutModule, 
  NbButtonModule, NbIconModule, NbInputModule, NbContextMenuModule, NbUserModule, NbSpinnerModule } from '@nebular/theme/';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { AccountComponent } from './account/account.component';
import { AccountModule } from './account/account.module';
import { SearchModule} from './search/search.module';
import { StatsComponent } from './stats/stats.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RepositoryComponent,
    AccessapiComponent,
    DatasetsComponent,
    OntologieComponent,
    PublishComponent,
    SettingfdpComponent,
    SettingsmartapiComponent,
    UserComponent,
    RepositoryinfoComponent,
    SwaggereditorComponent,
    ElasticsearchComponent,
    PublishApiComponent,
    SearchComponent,
    AccountComponent,
    StatsComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatDividerModule,
    MatCardModule,
    MatInputModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FileSaverModule,
    NbStepperModule,
    NbCardModule,
    NbThemeModule.forRoot(),
    NbLayoutModule,
    NbSidebarModule.forRoot(),
    NbContextMenuModule,
    NbButtonModule,
    NbMenuModule.forRoot(),
    NbIconModule,
    NbEvaIconsModule,
    NbInputModule,
    NbUserModule,
    AccountModule,
    SearchModule,
    NbSpinnerModule
  ],
  providers: [
    AppConfiguration,
    ParseXmlService,
    { 
        provide: APP_INITIALIZER, 
        useFactory: AppConfigurationFactory, 
        deps: [AppConfiguration, HttpClient], multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function AppConfigurationFactory(appConfig: AppConfiguration) {
    return () => appConfig.ensureInit();
  }