import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfiguration } from '../AppConfiguration';
import { FileSaverService } from 'ngx-filesaver';
import { environment } from 'src/environments/environment.prod';


@Component({
  selector: 'app-datasets',
  templateUrl: './datasets.component.html',
  styleUrls: ['./datasets.component.scss']
})


export class DatasetsComponent implements OnInit {
  dataresult: any;
  itemsdatasets: any;

  constructor(
    private appConfig: AppConfiguration,
    private http: HttpClient,
    private _FileSaverService: FileSaverService,
  ) { }

  ngOnInit() {

  }

  listdatasets() {
    var myHeaders = new Headers();
        myHeaders.append("Content-Type", "Application/json");
    var myInit = { method: 'GET', headers: myHeaders};
    
    
    //appeler smart havester pour récuperer l'api à lancer


    // https://data.inrae.fr/api/search?q=*&per_page=10&type=dataset&start=16000&show_entity_ids=true&show_my_data=true

  //  var myRequest = new Request(this.appConfig.smartapiurl+'harvest/allurls',myInit);
      var myRequest = new Request('https://dataverse.ird.fr/api/search?q=*&per_page=100&show_facets=true&show_my_data=true',myInit);

          fetch(myRequest, myInit)
          .then(response => {          
              response.json()
                  .then(data => {
                    //this.dataresult =data;                    
                    this.itemsdatasets = data['data']['items'];                         
                    console.log(this.itemsdatasets);
                    for (var i = 0; i < this.itemsdatasets.length; i++) {
                      this.createdataset(data['data']['items'][i]);
                    }
                  });            
          });    
          return null; 
  }  


  createdataset(item: any) {
    console.log(item);
let data: string;
let description: string;
description = JSON.stringify(item['description']);
let name: string;
name = JSON.stringify(item['name']);
let url: string;
url = JSON.stringify(item['url']);

data ='\
@prefix dcat: <http://www.w3.org/ns/dcat#>.\n\
@prefix dct: <http://purl.org/dc/terms/>.\n\
@prefix language: <http://id.loc.gov/vocabulary/iso639-1/>.\n\
@prefix s: <'+this.appConfig.fdpurl+'/>.\n\
@prefix c: <'+this.appConfig.fdpurl+'/catalog/>.\n\
\n\
s:new\n\
    a dcat:Dataset, dcat:Resource;\n\
    dct:description '+ description+ ';\n\
    dct:hasVersion "1.0";\n\
    dct:isPartOf c:a21f9b06-b7e7-43c0-869d-d81f09053383;\n\
    dct:language language:en;\n\
    dct:license <http://rdflicense.appspot.com/rdflicense/cc-by-nc-nd3.0>;\n\
    dct:title '+ name +';\n\
    dcat:keyword '+ url +'.\n'

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept':  'text/turtle',
        'Content-Type':  'text/turtle',
        'Authorization': 'Bearer '+ environment.token
      })
    };

    return this.http.post(this.appConfig.fdpurl+"/dataset", data, httpOptions ).subscribe( (r)=>{console.log('got r', r)}) ;

  }


}
