import { Component, OnInit } from '@angular/core';
import { AppConfiguration } from '../AppConfiguration';
import { HttpClient } from '@angular/common/http';
import { FileSaverService } from 'ngx-filesaver';
import { FormControl} from '@angular/forms';
import { FormGroup} from '@angular/forms';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-elasticsearch',
  templateUrl: './elasticsearch.component.html',
  styleUrls: ['./elasticsearch.component.scss']
})
export class ElasticsearchComponent implements OnInit {


  Form = new FormGroup({
    elasticurl: new FormControl(),
});

  constructor(
    private appConfig: AppConfiguration,
    private http: HttpClient,
    private _FileSaverService: FileSaverService
    ) {}

  ngOnInit() {
    this.Form.setValue({
      elasticurl: this.appConfig.elasticurl  ,
    });
  }


  SaveElasticsearchSetting() {
      let data: string;
      data ='\
        {\n\
          "fdpurl": "'+ this.appConfig.fdpurl +'", \n\
          "fdpemail": "'+ this.appConfig.fdpemail +'", \n\
          "fdppassword": "'+ this.appConfig.fdppassword +'", \n\
          "elasticurl": "'+ this.Form.value.elasticurl +'", \n\
          "smartapiurl": "'+ this.appConfig.smartapiurl +'" \n\
        }'
        console.log(data);      
        return this.http.post("http://"+environment.apiurl+"/api/setting", data,{responseType: 'text'}).subscribe( (r)=>{console.log(r)}) ;  
    };
  

}
