import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { shareReplay,catchError } from "rxjs/operators";
import { User } from 'src/app/user/model/user';
import { Cookie } from 'ng2-cookies';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public clientId = 'newClient';
  public redirectUri = 'http://localhost:8089/';

  constructor(private http: HttpClient) { }

  retrieveToken(code) {
    let params = new URLSearchParams();   
    params.append('grant_type','authorization_code');
    params.append('client_id', this.clientId);
    params.append('client_secret', 'newClientSecret');
    params.append('redirect_uri', this.redirectUri);
    params.append('code',code);
 
    let headers = 
      new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8'});
       
    this.http.post('http://localhost:8083/auth/smartharvester/protocol/openid-connect/token', 
                    params.toString(), { headers: headers })
        .subscribe( data => this.saveToken(data), err => alert('Invalid Credentials')); 
  }

  login(email:string, password:string ) {
    return this.http.post<User>('/api/login', {email, password})
       .pipe(
        // defaults to all values so we set it to just keep and replay last one
          shareReplay(1))
  }

  saveToken(token) {
    let expireDate = new Date().getTime() + (1000 * token.expires_in);
    Cookie.set("access_token", token.access_token, expireDate);
    console.log('Obtained Access token');
    window.location.href = 'http://localhost:8089';
  }
 
  getResource(resourceUrl) : Observable<any> {
    var headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8', 
      'Authorization': 'Bearer '+ Cookie.get('access_token')});
    return this.http.get(resourceUrl, { headers: headers })
                    .pipe(
                      catchError(
                        (error:any) => Observable.throw(error.json().error || 'Server error')
                        )
                    );
  }
 
  checkCredentials() {
    return Cookie.check('access_token');
  } 
 
  logout() {
    let token = Cookie.get('id_token');
    Cookie.delete('access_token');
    Cookie.delete('id_token');
    let logoutURL = "http://localhost:8083/auth/smartharvester/protocol/openid-connect/logout?id_token_hint="
      + token
      + "&post_logout_redirect_uri=" + this.redirectUri;

    window.location.href = logoutURL;
  } 

  public isLoggedIn(){
    return localStorage.getItem('ACCESS_TOKEN') !== null;

  }

}
