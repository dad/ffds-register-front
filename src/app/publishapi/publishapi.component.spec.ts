import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishapiComponent } from './publishapi.component';

describe('PublishapiComponent', () => {
  let component: PublishapiComponent;
  let fixture: ComponentFixture<PublishapiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishapiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishapiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
