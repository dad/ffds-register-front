import { ComponentFactoryResolver, OnDestroy, ViewChild } from '@angular/core';
import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { Directive, ViewContainerRef } from '@angular/core';





@Component({
  selector: 'app-publishapi',
  templateUrl: './publishapi.component.html',
  styleUrls: ['./publishapi.component.scss']
})
export class PublishApiComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  
  }

  goToLink()  {
    window.open("/swaggerapi");
  }

}
