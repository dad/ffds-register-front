import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FileSaverService } from 'ngx-filesaver';
import { FormControl} from '@angular/forms';
import { FormGroup} from '@angular/forms';
import { AppConfiguration } from '../AppConfiguration';
import { environment } from 'src/environments/environment.prod';


@Component({
  selector: 'app-repository',
  templateUrl: './repository.component.html',
  styleUrls: ['./repository.component.scss']
})

export class RepositoryComponent implements OnInit {

  public fileName: string;
  public importFile: File;
  public resultat: any;

 Form = new FormGroup({
            repotype: new FormControl(),
            reponame: new FormControl('', Validators.required),
            repodescription: new FormControl(),
            repourl: new FormControl(),
            repoversion: new FormControl(),
            repolicence: new FormControl(),
            repolanguage: new FormControl(),
            filetofill: new FormControl(),
  });


  constructor(
    private appConfig: AppConfiguration,
  private http: HttpClient,
  private _FileSaverService: FileSaverService,
  ) { }

  ngOnInit() {
  }

  repositorytoyaml(buttonType) {
if (buttonType == 'submit') {
let data: string;


data ='\
repository:\n\
  type: '+ this.Form.value.repotype +'\n\
  title: '+ this.Form.value.reponame +'\n\
  description: '+ this.Form.value.repodescription +'\n\
  url: '+ this.Form.value.repourl +'\n\
  version: '+ this.Form.value.repoversion +' \n\
  licence: '+ this.Form.value.repolicence +'\n\
  language: '+ this.Form.value.repolanguage +'\n\
'

    const fileName = `repository.yaml`;
    const fileType = this._FileSaverService.genType(fileName);
    const txtBlob = new Blob([data], { type: fileType });

    this._FileSaverService.save(txtBlob, fileName);
  }

  if (buttonType == 'fill') {
     const files = (event.target as HTMLInputElement).files;
     this.importFile = files[0];

    let text = "";
    let lines = [];
    let line = "";
    let map = new Map();
    let fileReader = new FileReader();


    fileReader.onload = (event) => {
    text = fileReader.result as string;

      lines = text.split("\n");
      for ( let i = 0; i < lines.length; i++) {
           line = lines[i].split(": ");
           map.set(line[0].trim(),line[1]);
      }

      this.Form.setValue({
        repotype:  map.get('type') ,
        reponame:  map.get('title') ,
        repodescription: map.get('description'),
        repourl: map.get('url'),
        repoversion: map.get('version'),
        repolicence: map.get('licence'),
        repolanguage: map.get('language'),
        filetofill: ''
      });

    }
    fileReader.readAsText(this.importFile);
   }

   if (buttonType == 'publish') {

    let data: string;

    data ='\
@prefix dcat: <http://www.w3.org/ns/dcat#>.\n\
@prefix dct: <http://purl.org/dc/terms/>.\n\
@prefix fdp: <'+this.appConfig.fdpurl+'/>.\n\nfdp:new \n\
    a dcat:Catalog, dcat:Resource;\n\
    dct:description "'+ this.Form.value.repodescription + '";\n\
    dct:hasVersion "'+ this.Form.value.repoversion + '";\n\
    dct:isPartOf <'+this.appConfig.fdpurl+'>;\n\
    dcat:keyword "'+this.Form.value.repotype+'";\n\
    dct:title "'+ this.Form.value.reponame + '".\n'

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept':  'text/turtle',
        'Content-Type':  'text/turtle',
        'Authorization': 'Bearer '+ environment.token
      })
    };

       this.resultat = this.http
      .post(this.appConfig.fdpurl+"/catalog", data, httpOptions )
      .subscribe( (r)=>{console.log('reponse: ', r)}) ;

      console.log("resultat: " + JSON.stringify(this.resultat));

      return JSON.stringify(this.resultat);


   }


  }


}
