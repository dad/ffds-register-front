export interface Head {
    vars: string[];
}
export interface Results {
    bindings: any[];
}

export interface SearchResult {
    head: Head;
    results: Results;
}