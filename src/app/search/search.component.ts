import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ParseXmlService } from '../services/parse-xml.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';


export interface formData{
  [key: string]: string;
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {


  public results: string[] = []; 
  firstTime = true;
  searchForm;
  searchedTerm;


  constructor(private parserService: ParseXmlService, private formBuilder: FormBuilder) {
    this.searchForm = this.formBuilder.group({
      inputSearchTerm: '',
    });
   }

  ngOnInit(): void {
  }

  loading = false;

  hasResults() {
    this.loading = this.results.length > 0;
    setTimeout(() => this.loading = false, 2000);
  }

  simpleSearch(formData){
    this.firstTime = false;
    //curl -X POST -H 'Accept: application/rdf+xml' -i 'http://10.6.10.9:8888/blazegraph/sparql' --data 'query=SELECT * where {?s a <http://www.w3.org/ns/dcat#Dataset> }'
    let term = formData["inputSearchTerm"]
    this.searchedTerm = term;
    let query='query=PREFIX dcat: <http://www.w3.org/ns/dcat#>\n\
              PREFIX dcterms: <http://purl.org/dc/terms/>\n\
              SELECT  ?title ?description ?uri \n\
              where {\n\?dataset a dcat:Dataset ;\n\
                        dcterms:title ?title ;\n\
                        dcterms:description ?description; \n\
                        dcat:keyword ?uri ; \n\
                        FILTER (contains( ?description, "' + 
                        term +'") || contains( ?title, "'+ term +'"))\n\.\n\
              }'
    
    this.parserService.getXmlResult(query).subscribe(
      data=>{
        if (data){
          this.results = [];
          data.results.bindings.forEach(element => {
            this.results.push(element);
          });
        }
      })
      this.searchForm.reset();
  }


  isXmlItemsEmpty(){
    return this.results.length === 0;
  } 

}
