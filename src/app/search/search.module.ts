import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Head,Results,SearchResult } from './models/search-result.model';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class SearchModule { }
