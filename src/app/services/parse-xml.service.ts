import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ParseXmlService {
 
  blazePath = "http://51.210.211.132:8888/blazegraph/sparql"

  constructor(private http:HttpClient) { }
  
  
  getXmlResult(body): Observable<any> {
    const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/x-www-form-urlencoded',
          'responseType': 'application/sparql-results+xml'         
        })
      };
    return this.http.post(this.blazePath,body, httpOptions);   
  }

}
