import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingfdpComponent } from './settingfdp.component';

describe('SettingfdpComponent', () => {
  let component: SettingfdpComponent;
  let fixture: ComponentFixture<SettingfdpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingfdpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingfdpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
