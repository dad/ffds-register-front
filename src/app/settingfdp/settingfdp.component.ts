import { Component, OnInit } from '@angular/core';
import { AppConfiguration } from '../AppConfiguration';
import { HttpClient } from '@angular/common/http';
import { FileSaverService } from 'ngx-filesaver';
import { FormControl} from '@angular/forms';
import { FormGroup} from '@angular/forms';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-settingfdp',
  templateUrl: './settingfdp.component.html',
  styleUrls: ['./settingfdp.component.scss']
})

export class SettingfdpComponent implements OnInit {
 
  Form = new FormGroup({
    title: new FormControl(),
    fdpurl: new FormControl(),
    fdpemail: new FormControl(),
    fdppassword: new FormControl(),
});

  constructor(
    private appConfig: AppConfiguration,
    private http: HttpClient,
    private _FileSaverService: FileSaverService
    ) {}

  ngOnInit() {
    this.Form.setValue({
      title: this.appConfig.title  ,
      fdpurl: this.appConfig.fdpurl  ,
      fdpemail: this.appConfig.fdpemail  ,
      fdppassword: this.appConfig.fdppassword  
    });
  }


  SaveFdpSetting() {
    let data: string;
    data ='\
      {\n\
        "title": "'+ this.Form.value.title + '", \n\
        "fdpurl": "'+ this.Form.value.fdpurl + '", \n\
        "fdpemail": "'+ this.Form.value.fdpemail + '", \n\
        "fdppassword": "'+ this.Form.value.fdppassword + '", \n\
        "elasticurl": "' + this.appConfig.elasticurl + '", \n\
        "smartapiurl": "' + this.appConfig.smartapiurl + '" \n\
      }'
      console.log(data);      
      return this.http.post("http://"+environment.apiurl+"/setting", data,{responseType: 'text'}).subscribe( (r)=>{console.log(r)}) ;  
  };

  
  }


  


