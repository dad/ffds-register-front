import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsmartapiComponent } from './settingsmartapi.component';

describe('SettingsmartapiComponent', () => {
  let component: SettingsmartapiComponent;
  let fixture: ComponentFixture<SettingsmartapiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsmartapiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsmartapiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
