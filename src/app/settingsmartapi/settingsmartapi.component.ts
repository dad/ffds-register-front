import { Component, OnInit } from '@angular/core';
import { AppConfiguration } from '../AppConfiguration';
import { HttpClient } from '@angular/common/http';
import { FileSaverService } from 'ngx-filesaver';
import { FormControl} from '@angular/forms';
import { FormGroup} from '@angular/forms';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-settingsmartapi',
  templateUrl: './settingsmartapi.component.html',
  styleUrls: ['./settingsmartapi.component.scss']
})
export class SettingsmartapiComponent implements OnInit {


  Form = new FormGroup({
    smartapiurl: new FormControl(),
});

  constructor(
    private appConfig: AppConfiguration,
    private http: HttpClient,
    private _FileSaverService: FileSaverService
    ) {}

  ngOnInit() {
    this.Form.setValue({
      smartapiurl: this.appConfig.smartapiurl
    });
  }


  SaveSmartapiSetting() {
    let data: string;
    data ='\
      {\n\
        "fdpurl": "'+ this.appConfig.fdpurl +'", \n\
        "fdpemail": "'+ this.appConfig.fdpemail +'", \n\
        "fdppassword": "'+ this.appConfig.fdppassword +'", \n\
        "elasticurl": "'+ this.appConfig.elasticurl +'", \n\
        "smartapiurl": "'+ this.Form.value.smartapiurl +'" \n\
      }'
      console.log(data);      
      return this.http.post("http://"+environment.apiurl+"/setting", data,{responseType: 'text'}).subscribe( (r)=>{console.log(r)}) ;  
  };

}
