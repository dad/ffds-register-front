import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ParseXmlService } from '../services/parse-xml.service';


@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {
  
  public results: string[] = []; 
  public stats: string[] = [];

  constructor(private parserService: ParseXmlService) { }

  ngOnInit(): void {

      let query1='query=SELECT (COUNT(?s) AS ?triples) WHERE { ?s a <http://www.w3.org/ns/dcat#Catalog> }'
      this.parserService.getXmlResult(query1).subscribe(data=>{if (data){this.results = []; data.results.bindings.forEach(element => { this.results.push(element);});
      this.stats.push(this.results[0]["triples"].value)

      let query2='query=SELECT (COUNT(?s) AS ?triples) WHERE { ?s a <http://www.w3.org/ns/dcat#Dataset> }'
      this.parserService.getXmlResult(query2).subscribe(data=>{if (data){this.results = []; data.results.bindings.forEach(element => { this.results.push(element);});
      this.stats.push(this.results[0]["triples"].value); }})

      console.log(this.stats);
    }})

    }




}
